or primera vez, se puede observar con gran nitidez una imagen inédita de la producción de polvo por el choque de viento de dos estrellas masivas, tomadas con el Telescopio Espacial James Webb (JWST, por sus siglas en inglés).

Joel Sánchez Bermúdez, investigador del Instituto de Astronomía (IA), es el único mexicano en participar en esta colaboración internacional, que agrupa a 32 investigadores de 34 instituciones de ocho países: Estados Unidos, Reino Unido, Canadá, Australia, Francia, Alemania, Japón y México. El grupo científico está encabezado por Ryan M. Lau, del NSF’s NOIR Lab, ubicado en Tucson, Arizona, y perteneciente a la Fundación Nacional de Ciencias de Estados Unidos.

Las estrellas que hay en el universo tienen diferentes masas. Hay unas como el Sol, que son relativamente pequeñas. Las que tienen al menos ocho veces la masa del Sol se consideran masivas o de alta masa, explicó Sánchez Bermúdez.

“Esas estrellas son muy importantes para la evolución química del universo, porque generan la mayoría de los elementos químicos pesados que existen, ya sea a lo largo de su vida o a través de su muerte en forma de explosiones de supernovas”, detalló.

Las masivas tienen una característica muy particular: 90 por ciento de ellas están en sistemas múltiples o binarios. Eso quiere decir que hay al menos dos estrellas orbitando una alrededor de la otra, a diferencia de las estrellas de baja masa, como el Sol, en donde la proporción de sistemas múltiples es menor.